// API DEL MUNDIAL RUSIA 2018 - CAMBIAR SI ES NECESARIO 
$.getJSON('https://raw.githubusercontent.com/lsv/fifa-worldcup-2018/master/data.json', function (url) {
                    
                    
    // Equipos Primeros de Grupo 
    primeroA = url.knockout.round_16.matches[0].home_team; // Ganador del Grupo A
    primeroB = url.knockout.round_16.matches[2].home_team; // Ganador del Grupo B
    primeroC = url.knockout.round_16.matches[1].home_team; // Ganador del Grupo C
    primeroD = url.knockout.round_16.matches[3].home_team; // Ganador del grupo D
    primeroE = url.knockout.round_16.matches[4].home_team; // Ganador del grupo E
    primeroF = url.knockout.round_16.matches[6].home_team; // Ganador del grupo F
    primeroG = url.knockout.round_16.matches[5].home_team; // Ganador del grupo G
    primeroH = url.knockout.round_16.matches[7].home_team; // Ganador del grupo H

    // Equipos Segundos de Grupo
    segundoA = url.knockout.round_16.matches[2].away_team; // Segundo del grupo A
    segundoB = url.knockout.round_16.matches[0].away_team; // Segundo del Grupo B
    segundoC = url.knockout.round_16.matches[3].away_team; // Segundo del Grupo C
    segundoD = url.knockout.round_16.matches[1].away_team; // Segundo del Grupo D
    segundoE = url.knockout.round_16.matches[6].home_team; // Segundo del grupo E
    segundoF = url.knockout.round_16.matches[4].away_team; // Segundo del grupo F
    segundoG = url.knockout.round_16.matches[7].home_team; // Segundo del grupo G
    segundoH = url.knockout.round_16.matches[5].away_team; // Segundo del grupo H

    // Marcadores de los Partidos de la Fase de Octavos
    marcador1A = url.knockout.round_16.matches[0].home_result; // Goles 1A
    marcador2B = url.knockout.round_16.matches[0].away_result; // Goles 2B

    marcador1C = url.knockout.round_16.matches[1].home_result; // Goles 1C
    marcador2D = url.knockout.round_16.matches[1].away_result; // Goles 2D

    marcador1E = url.knockout.round_16.matches[4].home_result; // Goles 1E
    marcador2F = url.knockout.round_16.matches[4].away_result; // Goles 2F

    marcador1G = url.knockout.round_16.matches[5].home_result; // Goles 1G
    marcador2H = url.knockout.round_16.matches[5].away_result; // Goles 2H

    marcador1B = url.knockout.round_16.matches[2].home_result; // Goles 1B
    marcador2A = url.knockout.round_16.matches[2].away_result; // Goles 2A

    marcador1D = url.knockout.round_16.matches[3].home_result; // Goles 1D
    marcador2C = url.knockout.round_16.matches[3].away_result; // Goles 2C

    marcador1F = url.knockout.round_16.matches[6].home_result; // Goles 1F
    marcador2E = url.knockout.round_16.matches[6].away_result; // Goles 2E

    marcador1H = url.knockout.round_16.matches[7].home_result; // Goles 1H
    marcador2G = url.knockout.round_16.matches[7].away_result; // Goles 2G

});