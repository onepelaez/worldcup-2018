<?php
require_once("../conf/Configuracion.php");
require_once("../core/ConectorMySQLi.php");

class Usuarios {
	
    function __construct() {
        $this->conector = new ConectorMySQLi(Variables::$HOST_BD, Variables::$NOMBRE_BD,
        									 Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }

    function agregarUsuario($codJugadorPolla,$fechapolla,
    					 $identificacion,$nombresJugadorPolla,
    					 $email,$movil){
       
        $row=$this->conector->obtenerAssoc( "   SELECT  count(*) cantidad  ,
                                                        max(identificacion_jugador) identificacion_jugador
                                                FROM codigos_pollas
                                                WHERE codjugadorpolla = '".$codJugadorPolla."'
                                                group by identificacion_jugador ");


        if(isset($row[0])){
            if($row[0]['cantidad']==0){
                $RetornoFun=array(  "code"=>"400",
                                    "msj"=>"Codigo de polla no existe");
                return json_encode($RetornoFun) ;
                die();
            }else{
                if($row[0]['identificacion_jugador'] != NULL){
                    $RetornoFun=array(  "code"=>"400",
                                        "msj"=>"Codigo de polla tiene usuario registrado");
                    return json_encode($RetornoFun) ;
                    die();
                }
            }
        }else{
            $RetornoFun=array(  "code"=>"400",
                                    "msj"=>"Codigo de polla no existe");
                return json_encode($RetornoFun) ;
                die();
        }


        $codigo=$this->conector->obtenerAssoc(" select count(*) cantidad,
        										max(identificacion_jugador) identificacion_jugador
                                                from  codigos_pollas  
                                                WHERE codjugadorpolla = '".$codJugadorPolla."' 
                                                group by codjugadorpolla");        
        if($codigo[0]['cantidad'] > 0){

            if($codigo[0]['identificacion_jugador'] == NULL ){

                $row=$this->conector->obtenerAssoc("select count(*) cantidad
                                                    from  jugadorpolla 
                                                    WHERE codJugadorPolla = '".$codJugadorPolla."'");
                if($row[0]['cantidad']>0){
                    $envioBD=array("code"=>"400","Codigo de jugador ya tiene usuario");
                }else{
                    $consulta='INSERT INTO  jugadorpolla ( codJugadorPolla,fechapolla,
                                                        identificacion,nombresJugadorPolla,
                                                        email,movil) 
                                                VALUES (\''.$codJugadorPolla.'\',\''.$fechapolla.'\',\''
                                                        .$identificacion.'\',\''.$nombresJugadorPolla.'\',\''
                                                        .$email.'\',\''.$movil.'\')';
                                                        
                                                        
                    if($this->conector->consultar($consulta)){
                        
                        $UPDconsulta=' UPDATE  jugadorpolla
                                       SET identificacion_jugador =\''.$identificacion.'\'
                                       WHERE codjugadorpolla=\''.$codJugadorPolla.'\'';
                        if($this->conector->consultar($UPDconsulta)){
                            $envioBD=array("code"=>"200","msj"=>"Usuario Ingresado Con Exito");
                        }else{
                            $envioBD=array("code"=>"400","msj"=>"ERROR: Usuario No Ingresado ");
                        }
                    }else{
                        $envioBD=array("code"=>"400","msj"=>"ERROR: Usuario No Ingresado ");
                    }                                                                    
                }
            }else{
                $envioBD=array("code"=>"400","msj"=>"ERROR: Codigo de Polla Ya Tiene Un Usuario ");
            }
        }else{
            $envioBD=array("code"=>"400","msj"=>"ERROR: Polla No Existe ");

        }
         return json_encode($envioBD);
	}

	public function verificarUsuario($codJugadorPolla,$identificacion,$email){
       	$row=$this->conector->obtenerAssoc("select count(*) cantidad
                                                    from  jugadorpolla 
                                                    WHERE codJugadorPolla = '".$codJugadorPolla."'
                                                    AND identificacion = '".$identificacion."'
                                                    AND email = '".$email."'");


		if($row[0]['cantidad']>0){
            $envioBD=array(	"code"=>"200",
            				"msj"=>"Ingreso permitido al usuario",
            				"codJugadorPolla"=>$codJugadorPolla);
		}else{
            $envioBD=array("code"=>"400","msj"=>"ERROR: Usuario No Registrado ");
		}
		return json_encode($envioBD);
	}


	public function puntuacionUsuarios(){
       	$row=$this->conector->obtenerAssoc("select * from  jugadorpolla ORDER BY puntosTotal DESC ");
        $envioBD=array(	"code"=>"200",
        				"msj"=>"Datos de las personas que tienen polla",
        				"data"=>$row);
		
		return json_encode($envioBD);
	}


}

?>