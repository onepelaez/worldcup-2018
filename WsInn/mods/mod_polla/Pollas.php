<?php
require_once("../conf/Configuracion.php");
require_once("../core/ConectorMySQLi.php");

class Pollas {
	
    function __construct() {
        $this->conector = new ConectorMySQLi(Variables::$HOST_BD, Variables::$NOMBRE_BD,
        									 Variables::$USUARIO_BD, Variables::$CLAVE_BD);
    }

    function getJsonHttp(){
        $url = "http://innovationtics.xyz/Inn/Polla/data.json";
        $json = file_get_contents($url);
        $obj = json_decode($json);
    
        $teams=$obj->teams;
        $groups=array("a","b","c","d","e","f","g","h");
    
        for ($group=0; $group < count($groups) ; $group++) { 
            $grupoVal=$groups[$group];
            $grupo=$obj->groups->$grupoVal;
            $grupoAMatches=$grupo->matches;
            $arrayGroupsMatches=array();
            for ($j=0; $j <count($grupoAMatches); $j++) { 
                $home_team=$grupoAMatches[$j]->home_team-1;
                $away_team=$grupoAMatches[$j]->away_team-1;
    
                if(!isset($teams[$home_team]->points_groups)){
                    $teams[$home_team]->points_groups=0;
                    $teams[$home_team]->group=$grupoVal;
                    $teams[$home_team]->groupId=$group+1;
                }
    
                if(!isset($teams[$away_team]->points_groups)){
                    $teams[$away_team]->points_groups=0;
                    $teams[$away_team]->group=$grupoVal;
                    $teams[$away_team]->groupId=$group+1;
                }
    
                $home_result=$grupoAMatches[$j]->home_result;
                $away_result=$grupoAMatches[$j]->away_result;
                if($home_result!="" && $away_result!=""){
                    if(	$home_result == $away_result){
                        $teams[$home_team]->points_groups=$teams[$home_team]->points_groups+1;
                        $teams[$away_team]->points_groups=$teams[$away_team]->points_groups+1;
                    }else if($home_result > $away_result){
                        $teams[$home_team]->points_groups=$teams[$home_team]->points_groups+3;
                    }else{
                        $teams[$away_team]->points_groups=$teams[$away_team]->points_groups+3;
                    }
                }
                
            }
        }
    
        $arrTeamGroup=array();
        for ($contTeam=0; $contTeam < count($teams) ; $contTeam++) { 
            $arrTeamGroup[$teams[$contTeam]->group][]=array("idteam"=>$teams[$contTeam]->id,
                                                            "nameteam"=>$teams[$contTeam]->name,
                                                            "pointsteam"=>$teams[$contTeam]->points_groups,
                                                            "groupId"=>$teams[$contTeam]->groupId);
            
        }
        
        
        
        $knockout = $obj->knockout;
        $round_16=$knockout->round_16;
        $MatRound16=$round_16->matches;
        
        $round_8=$knockout->round_8;
        $MatRound8=$round_8->matches;
        $round_4=$knockout->round_4;
        $MatRound4=$round_4->matches;
        $round_2_loser=$knockout->round_2_loser;
        $MatRound2Loser=$round_2_loser->matches;
        $round_2=$knockout->round_2;
        $MatRound2=$round_2->matches;
        

        $contadorPartidos=1;
        
        //partidos=array();
        for ($MatchC16=0; $MatchC16 < count($MatRound16) ; $MatchC16++) { 
        
            if($MatRound16[$MatchC16]->home_result ==""){
            $MatRound16[$MatchC16]->home_team="";
            $MatRound16[$MatchC16]->away_team="";
            }
            $partidos[]=array(
                            "cod_partido"=>$contadorPartidos,
                            "home_team"=>$MatRound16[$MatchC16]->home_team,
                            "away_team"=>$MatRound16[$MatchC16]->away_team,	
                            "home_result"=>$MatRound16[$MatchC16]->home_result,
                            "away_result"=>$MatRound16[$MatchC16]->away_result,	
                            "home_penalty"=>$MatRound16[$MatchC16]->home_penalty,
                            "away_penalty"=>$MatRound16[$MatchC16]->away_penalty,	
                            "winner"=>$MatRound16[$MatchC16]->winner,
                            "round"=>"16"
                            );
            $contadorPartidos++;
        }
        for ($MatchC8=0; $MatchC8 < count($MatRound8) ; $MatchC8++) { 
        
            if($MatRound8[$MatchC8]->home_result ==""){
            $MatRound8[$MatchC8]->home_team="";
            $MatRound8[$MatchC8]->away_team="";
            }
            $partidos[]=array(
                            "cod_partido"=>$contadorPartidos,
                            "home_team"=>$MatRound8[$MatchC8]->home_team,
                            "away_team"=>$MatRound8[$MatchC8]->away_team,	
                            "home_result"=>$MatRound8[$MatchC8]->home_result,
                            "away_result"=>$MatRound8[$MatchC8]->away_result,	
                            "home_penalty"=>$MatRound8[$MatchC8]->home_penalty,
                            "away_penalty"=>$MatRound8[$MatchC8]->away_penalty,	
                            "winner"=>$MatRound8[$MatchC8]->winner,
                            "round"=>"8"
                            );
            $contadorPartidos++;        
        }
        
        for ($MatchC4=0; $MatchC4 < count($MatRound4) ; $MatchC4++) { 
        
            if($MatRound4[$MatchC4]->home_result ==""){
            $MatRound4[$MatchC4]->home_team="";
            $MatRound4[$MatchC4]->away_team="";
            }
            $partidos[]=array(
                            "cod_partido"=>$contadorPartidos,
                            "home_team"=>$MatRound4[$MatchC4]->home_team,
                            "away_team"=>$MatRound4[$MatchC4]->away_team,	
                            "home_result"=>$MatRound4[$MatchC4]->home_result,
                            "away_result"=>$MatRound4[$MatchC4]->away_result,	
                            "home_penalty"=>$MatRound4[$MatchC4]->home_penalty,
                            "away_penalty"=>$MatRound4[$MatchC4]->away_penalty,	
                            "winner"=>$MatRound4[$MatchC4]->winner,
                            "round"=>"4"
                            );
            $contadorPartidos++;        
        }

        for ($MatchC2=0; $MatchC2 < count($MatRound2) ; $MatchC2++) { 
        
            if($MatRound2[$MatchC2]->home_result ==""){
            $MatRound2[$MatchC2]->home_team="";
            $MatRound2[$MatchC2]->away_team="";
            }
            $partidos[]=array(
                            "cod_partido"=>$contadorPartidos,
                            "home_team"=>$MatRound2[$MatchC2]->home_team,
                            "away_team"=>$MatRound2[$MatchC2]->away_team,	
                            "home_result"=>$MatRound2[$MatchC2]->home_result,
                            "away_result"=>$MatRound2[$MatchC2]->away_result,	
                            "home_penalty"=>$MatRound2[$MatchC2]->home_penalty,
                            "away_penalty"=>$MatRound2[$MatchC2]->away_penalty,	
                            "winner"=>$MatRound2[$MatchC2]->winner,
                            "round"=>"1"
                            );
            $contadorPartidos++; 
        }
        
        for ($MatchC2Loser=0; $MatchC2Loser < count($MatRound2Loser) ; $MatchC2Loser++) { 
        
            if($MatRound2Loser[$MatchC2Loser]->home_result ==""){
            $MatRound2Loser[$MatchC2Loser]->home_team="";
            $MatRound2Loser[$MatchC2Loser]->away_team="";
            }
            $partidos[]=array(
                            "cod_partido"=>$contadorPartidos,
                            "home_team"=>$MatRound2Loser[$MatchC2Loser]->home_team,
                            "away_team"=>$MatRound2Loser[$MatchC2Loser]->away_team,	
                            "home_result"=>$MatRound2Loser[$MatchC2Loser]->home_result,
                            "away_result"=>$MatRound2Loser[$MatchC2Loser]->away_result,	
                            "home_penalty"=>$MatRound2Loser[$MatchC2Loser]->home_penalty,
                            "away_penalty"=>$MatRound2Loser[$MatchC2Loser]->away_penalty,	
                            "winner"=>$MatRound2Loser[$MatchC2Loser]->winner,
                            "round"=>"2"
                            );
            $contadorPartidos++;        
        }
        

        
        for ($group=0; $group < count($groups) ; $group++) { 
            $grupoVal=$groups[$group];
            $grupo=$arrTeamGroup[$grupoVal];
        	$grupoObj=$obj->groups->$grupoVal;
            for ($groupCount=0; $groupCount < count($grupo); $groupCount++) {
            	$posicion='NULL';
            	if($grupoObj->winner==$grupo[$groupCount]['idteam']){$posicion=1;} 
            	if($grupoObj->runnerup==$grupo[$groupCount]['idteam']){$posicion=2;} 
            	if($grupoObj->positionthree==$grupo[$groupCount]['idteam']){$posicion=3;} 
            	if($grupoObj->positionfour==$grupo[$groupCount]['idteam']){$posicion=4;} 

                $gruposPos[]=array("groupId"=>$grupo[$groupCount]['groupId'],
                                    "idteam"=>$grupo[$groupCount]['idteam'],
                                    "puntuacion"=>$grupo[$groupCount]['pointsteam'],
                                	"posicion"=>$posicion);
            }
        
        }      



        for ($i=0; $i < count($gruposPos); $i++) { 
            $row=$this->conector->obtenerAssoc("select count(*) cantidad
                                                from grupo_pais 
                                                WHERE idgrupo = '".$gruposPos[$i]['groupId']
                                                ."' AND idpais='".$gruposPos[$i]['idteam']."'");

            if($row[0]['cantidad']>0){    
                
                $consulta="	UPDATE grupo_pais 
                			SET puntuacion ='".$gruposPos[$i]['puntuacion']."',
                				posicion = ".$gruposPos[$i]['posicion']."
                            WHERE idgrupo = '".$gruposPos[$i]['groupId']
                            ."' AND idpais='".$gruposPos[$i]['idteam']."'";

            }else{
                $consulta="INSERT INTO grupo_pais (idgrupo,idpais,puntuacion)
                           VALUES ('".$gruposPos[$i]['groupId']."','".$gruposPos[$i]['idteam']
                                     ."','".$gruposPos[$i]['puntuacion']."')";
            }


            $this->conector->consultar($consulta);
        }

        for ($i=0; $i < count($partidos); $i++) { 
            $row=$this->conector->obtenerAssoc("select count(*) cantidad
                                                from partido WHERE cod_partido = '".$partidos[$i]['cod_partido']."'");


                                     if($partidos[$i]['cod_partido']==""){$partidos[$i]['cod_partido']="NULL";}
                                     if($partidos[$i]['home_team']==""){$partidos[$i]['home_team']="NULL";}
                                     if($partidos[$i]['away_team']==""){$partidos[$i]['away_team']="NULL";}
                                     if($partidos[$i]['home_result']==""){$partidos[$i]['home_result']="NULL";}
                                     if($partidos[$i]['away_result']==""){$partidos[$i]['away_result']="NULL";}
                                     if($partidos[$i]['home_penalty']==""){$partidos[$i]['home_penalty']="NULL";}
                                     if($partidos[$i]['away_penalty']==""){$partidos[$i]['away_penalty']="NULL";}
                                     if($partidos[$i]['winner']==""){$partidos[$i]['winner']="NULL";}
                                    if($partidos[$i]['round']."')"==""){$partidos[$i]['round']="NULL";}

            if($row[0]['cantidad']>0){
                $consulta="	UPDATE partido 
                			SET 
								idPaisLocal = ".$partidos[$i]['home_team'].",
								idPaisVisitante = ".$partidos[$i]['away_team'].",
								golesPL = ".$partidos[$i]['home_result'].",
								golesPV = ".$partidos[$i]['away_result'].",
								penaltyPL = ".$partidos[$i]['home_penalty'].",
								penaltyPV = ".$partidos[$i]['away_penalty'].",
								winner = ".$partidos[$i]['winner'].",
								round = ".$partidos[$i]['round']."
                            WHERE cod_partido = '".$partidos[$i]['cod_partido']."'";
            }else{
                $consulta="INSERT INTO partido (cod_partido,idPaisLocal,idPaisVisitante,
                                              golesPL,golesPV,penaltyPL,penaltyPV,winner,round)
                           VALUES ('".$partidos[$i]['cod_partido']."',"
                                     .$partidos[$i]['home_team'].","
                                     .$partidos[$i]['away_team'].","
                                     .$partidos[$i]['home_result'].","
                                     .$partidos[$i]['away_result'].","
                                     .$partidos[$i]['home_penalty'].","
                                     .$partidos[$i]['away_penalty'].","
                                     .$partidos[$i]['winner'].","
                                    .$partidos[$i]['round'].")";
          
            }

            $this->conector->consultar($consulta);
        }
        
        $envioBD=array("code"=>"200","grupos"=>$gruposPos,"eliminatorias"=>$partidos);
        
        return json_encode($envioBD);
    }




    function agregarPollaUsuario($jsonGrupos,$jsonEliminatorias,$codJugadorPolla){
       
        $row=$this->conector->obtenerAssoc( "   SELECT  count(*) cantidad  
                                                FROM jugadorpolla
                                                WHERE codJugadorPolla = '".$codJugadorPolla."' 
                                                AND polla_registrada=1");

        if($row[0]['cantidad']>0){
            $RetornoFun=array(  "code"=>"400",
                                "msj"=>"Polla ya se encuentra registrada");
            return json_encode($RetornoFun) ;
            die();
        }


        // INICIO FILTRADO JSON GRUPOS
        $jsonRETURN=array("grupos"=>array());
        $DecJsonGrupos=json_decode($jsonGrupos);

        for ($i=0; $i < count($DecJsonGrupos); $i++) { 
            $grupoPaisPos=array("groupId"=>$DecJsonGrupos[$i][3]->groupId,
                                "idteam"=>$DecJsonGrupos[$i][0],
                                "posicion"=>$DecJsonGrupos[$i][2]);
            array_push($jsonRETURN['grupos'],$grupoPaisPos);           
        }
        // FIN FILTRADO JSON GRUPOS

        // INICIO FILTRADO JSON ELIMINATORIAS
        $jsonRETURNEl=array("eliminatorias"=>array());
        $DecJsonGrupos=json_decode($jsonEliminatorias);
        $rondas = $DecJsonGrupos->results[0];
        $cantidadPartidos=1;
        for ($jk=0; $jk < count($rondas); $jk++) { 


            for ($jki=0; $jki < count($rondas[$jk]); $jki++) { 
                if($jk==0){$nroRonda =16;}
                else if($jk==1){$nroRonda =8;}
                else if($jk==2){$nroRonda =4;}
                else if($jk==3){if($jki==0){$nroRonda =1;}else{$nroRonda =2;}}
                
                if($rondas[$jk][$jki][0]>$rondas[$jk][$jki][1]){$winner=$rondas[$jk][$jki][2];}else{$winner=$rondas[$jk][$jki][3];}

                $eliminatoriasPais=array(   
                                            "cod_partido"=>$cantidadPartidos,
                                            "home_team"=>$rondas[$jk][$jki][2],
                                            "away_team"=>$rondas[$jk][$jki][3],
                                            "home_result"=>$rondas[$jk][$jki][0],
                                            "away_result"=>$rondas[$jk][$jki][1],
                                            "round"=>$nroRonda,
                                            "winner"=>$winner
                                        );
                array_push($jsonRETURNEl['eliminatorias'],$eliminatoriasPais); 
                $cantidadPartidos++;
            }
        }
        /** EN DESARROLLO */
        // FIN FILTRADO JSON ELIMINATORIAS      

        for ($i=0; $i < count($jsonRETURN['grupos']) ; $i++) { 
           $insertGrupoJugador="INSERT INTO grupo_pais_jugadorpolla (codjugadorpolla,idgrupo,idpais,posicion) 
                                VALUES ('".$codJugadorPolla
                                        ."',".$jsonRETURN['grupos'][$i]['groupId']
                                        .",".$jsonRETURN['grupos'][$i]['idteam']
                                        .",".$jsonRETURN['grupos'][$i]['posicion'].")";

            $this->conector->consultar($insertGrupoJugador);


        }

        
        for ($j=0; $j < count($jsonRETURNEl['eliminatorias']) ; $j++) { 
            $insertEliminatoriaJugador="INSERT INTO partido_jugadorpolla (idPartido,codJugadorPolla,idPaisLocal,
                                                                   idPaisVisitante,golesLocal,golesVisitante,
                                                                   winner,round) 
                                        VALUES (".$jsonRETURNEl['eliminatorias'][$j]['cod_partido']
                                                .",'".$codJugadorPolla
                                                ."',".$jsonRETURNEl['eliminatorias'][$j]['home_team']
                                                .",".$jsonRETURNEl['eliminatorias'][$j]['away_team']
                                                .",".$jsonRETURNEl['eliminatorias'][$j]['home_result']
                                                .",".$jsonRETURNEl['eliminatorias'][$j]['away_result']
                                                .",".$jsonRETURNEl['eliminatorias'][$j]['winner']
                                                .",".$jsonRETURNEl['eliminatorias'][$j]['round'].")";

            $this->conector->consultar($insertEliminatoriaJugador);
           
         }

         $this->conector->consultar("UPDATE jugadorpolla SET polla_registrada=1 WHERE codJugadorPolla = '".$codJugadorPolla."'");



        $RetornoFun=array(  "code"=>"200",
                            "grupos"=>$jsonRETURN['grupos'],
                            "eliminatorias"=>$jsonRETURNEl['eliminatorias'],
                            "codJugadorPolla"=>$codJugadorPolla);

        return json_encode($RetornoFun) ;
    }

    public function actualizarPollas(){
        $row=$this->conector->obtenerAssoc("SELECT gp.idgrupo,gp.idpais,
        										   gp.posicion posicion_gp,
        										   gpj.posicion posicion_gpj,
        										   gpj.codjugadorpolla 
											FROM grupo_pais gp,grupo_pais_jugadorpolla gpj 
											where gp.idgrupo=gpj.idgrupo 
											AND gp.idpais=gpj.idpais
											order by gpj.codjugadorpolla ,
													 gp.idgrupo,gp.idpais,
													 posicion_gp");

       // $arrayUpdate=array();
        for ($i=0; $i < count($row); $i++) { 
        	$puntuacion=0;
        	if($row[$i]['posicion_gp']==1){
        		if($row[$i]['posicion_gp']==$row[$i]['posicion_gpj']){$puntuacion=5;}

        		if($row[$i]['posicion_gp']==$row[$i+1]['posicion_gpj'] &&
        		   $row[$i+1]['posicion_gp']==$row[$i]['posicion_gpj']){
        			$puntuacion=1.5;
        		}
        	}


        	if($row[$i]['posicion_gp']==2){
        		if($row[$i]['posicion_gp']==$row[$i]['posicion_gpj']){$puntuacion=3;}
        		if($row[$i]['posicion_gp']==$row[$i-1]['posicion_gpj'] &&
        		   $row[$i-1]['posicion_gp']==$row[$i]['posicion_gpj']){
        			$puntuacion=1.5;
        		}
        	}
        	if($row[$i]['posicion_gp']==3){
        		if($row[$i]['posicion_gp']==$row[$i]['posicion_gpj']){$puntuacion=1;}
        	}

	        /*array_push($arrayUpdate, array(	 
	        								 "codJugadorPolla"=>$row[$i]['codjugadorpolla']
    										,"idgrupo"=>$row[$i]['idgrupo']
    										,"idpais"=>$row[$i]['idpais']
    										,"posicion_gp"=>$row[$i]['posicion_gp']
    										,"posicion_gpj"=>$row[$i]['posicion_gpj']
    										,"puntuacion"=>$puntuacion
    									));*/

    		$UpdateGru="	UPDATE grupo_pais_jugadorpolla 
    				  		SET puntuacion ='".$puntuacion
    					."'	WHERE codjugadorpolla='".$row[$i]['codjugadorpolla']."'
    				   		AND idgrupo=".$row[$i]['idgrupo']
    					."  AND idpais=".$row[$i]['idpais'];
   			$this->conector->consultar($UpdateGru);
        }



   		$rowPar=$this->conector->obtenerAssoc("	SELECT p.idpartido,p.idpaislocal,p.idpaisvisitante,
													   p.golespl,p.golespv,p.winner,p.round,
													   pj.codjugadorpolla,pj.idpaislocal idpaislocalPJ,
													 pj.idpaisvisitante idpaisvisitantePJ,
													 pj.goleslocal goleslocalPJ,
													 pj.golesvisitante golesvisitantePJ,
													 pj.winner winnerPJ,pj.round roundPJ
												FROM partido p , partido_jugadorpolla pj 
												WHERE p.idpartido=pj.idPartido 
												order by pj.codjugadorpolla ,p.idpartido");


        //$arrayUpdatePar=array();
   		for ($j=0; $j <count($rowPar) ; $j++) { 
			$puntuacion=0;
			if($rowPar[$j]['roundPJ']==16){
				if($rowPar[$j]['winner']==$rowPar[$j]['winnerPJ']){$puntuacion=$puntuacion+7;}

				if($rowPar[$j]['golespl']==$rowPar[$j]['goleslocalPJ'] &&
				   $rowPar[$j]['golespv']==$rowPar[$j]['golesvisitantePJ']){
					$puntuacion=$puntuacion+4;
				}
				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaislocalPJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaisvisitantePJ']){
					$puntuacion=$puntuacion+3;
				}

				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaisvisitantePJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaislocalPJ']){
					$puntuacion=$puntuacion+3;
				}
			}

			if($rowPar[$j]['roundPJ']==8){
				if($rowPar[$j]['winner']==$rowPar[$j]['winnerPJ']){$puntuacion=$puntuacion+10;}
				if($rowPar[$j]['golespl']==$rowPar[$j]['goleslocalPJ'] &&
				   $rowPar[$j]['golespv']==$rowPar[$j]['golesvisitantePJ']){
					$puntuacion=$puntuacion+5;
				}
				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaislocalPJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaisvisitantePJ']){
					$puntuacion=$puntuacion+4;
				}
				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaisvisitantePJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaislocalPJ']){
					$puntuacion=$puntuacion+4;
				}
			}

			if($rowPar[$j]['roundPJ']==4){
				if($rowPar[$j]['winner']==$rowPar[$j]['winnerPJ']){$puntuacion=$puntuacion+15;}
				if($rowPar[$j]['golespl']==$rowPar[$j]['goleslocalPJ'] &&
				   $rowPar[$j]['golespv']==$rowPar[$j]['golesvisitantePJ']){
					$puntuacion=$puntuacion+6;
				}
				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaislocalPJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaisvisitantePJ']){
					$puntuacion=$puntuacion+4;
				}
				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaisvisitantePJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaislocalPJ']){
					$puntuacion=$puntuacion+4;
				}
			}

			if($rowPar[$j]['roundPJ']==2){
				if($rowPar[$j]['winner']==$rowPar[$j]['winnerPJ']){$puntuacion=$puntuacion+20;}
				if($rowPar[$j]['golespl']==$rowPar[$j]['goleslocalPJ'] &&
				   $rowPar[$j]['golespv']==$rowPar[$j]['golesvisitantePJ']){
					$puntuacion=$puntuacion+8;
				}
				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaislocalPJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaisvisitantePJ']){
					$puntuacion=$puntuacion+6;
				}
				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaisvisitantePJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaislocalPJ']){
					$puntuacion=$puntuacion+6;
				}
			}
			if($rowPar[$j]['roundPJ']==1){
				if($rowPar[$j]['winner']==$rowPar[$j]['winnerPJ']){$puntuacion=$puntuacion+30;}
				if($rowPar[$j]['golespl']==$rowPar[$j]['goleslocalPJ'] &&
				   $rowPar[$j]['golespv']==$rowPar[$j]['golesvisitantePJ']){
					$puntuacion=$puntuacion+10;
				}
				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaislocalPJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaisvisitantePJ']){
					$puntuacion=$puntuacion+7;
				}
				if($rowPar[$j]['idpaislocal']==$rowPar[$j]['idpaisvisitantePJ'] &&
				   $rowPar[$j]['idpaisvisitante']==$rowPar[$j]['idpaislocalPJ']){
					$puntuacion=$puntuacion+7;
				}
			}
	        /*array_push($arrayUpdatePar, array(	 
							 "codJugadorPolla"=>$rowPar[$j]['codjugadorpolla']
							,"idpartido"=>$rowPar[$j]['idpartido']
							,"idpaislocal"=>$rowPar[$j]['idpaislocal']
							,"idpaisvisitante"=>$rowPar[$j]['idpaisvisitante']
							,"golespl"=>$rowPar[$j]['golespl']
							,"golespv"=>$rowPar[$j]['golespv']
							,"winner"=>$rowPar[$j]['winner']
							,"round"=>$rowPar[$j]['round']
							,"idpaislocalPJ"=>$rowPar[$j]['idpaislocalPJ']
							,"idpaisvisitantePJ"=>$rowPar[$j]['idpaisvisitantePJ']
							,"goleslocalPJ"=>$rowPar[$j]['goleslocalPJ']
							,"golesvisitantePJ"=>$rowPar[$j]['golesvisitantePJ']
							,"winnerPJ"=>$rowPar[$j]['winnerPJ']
							,"roundPJ"=>$rowPar[$j]['roundPJ']
							,"puntuacion"=>$puntuacion
			));*/

			$UpdatePar="	UPDATE partido_jugadorpolla 
    				  		SET puntuacion ='".$puntuacion
    					."'	WHERE codjugadorpolla='".$rowPar[$j]['codjugadorpolla']."'
    						AND idpartido=".$rowPar[$j]['idpartido'];
   			$this->conector->consultar($UpdatePar);
   		}



   		$rowGru=$this->conector->obtenerAssoc("	SELECT sum(puntuacion) puntuacion,codjugadorpolla
												FROM grupo_pais_jugadorpolla 
												group by codjugadorpolla");
   		for ($k=0; $k < count($rowGru) ; $k++) { 
   			$UpdateRowGru="	UPDATE jugadorpolla 
    				  		SET puntosGrupos ='".$rowGru[$k]['puntuacion']
    					."'	WHERE codjugadorpolla='".$rowGru[$k]['codjugadorpolla']."'";

   			$this->conector->consultar($UpdateRowGru);
   		}


   		$RowEli=$this->conector->obtenerAssoc("	SELECT sum(puntuacion) puntuacion,codjugadorpolla 
   												FROM partido_jugadorpolla 
   												group by codjugadorpolla");

   		for ($l=0; $l < count($RowEli) ; $l++) { 
   			$UpdateRowEli="	UPDATE jugadorpolla 
    				  		SET puntosEliminatorias ='".$RowEli[$l]['puntuacion']
    					."',puntosTotal=puntosGrupos+".$RowEli[$l]['puntuacion']."
							WHERE codjugadorpolla='".$RowEli[$l]['codjugadorpolla']."'";

   			$this->conector->consultar($UpdateRowEli);
   		}




     	$RetornoFun=array(  "code"=>"200",
                            "msj"=>"Actualizacion realizada exitosamente");

        return json_encode($RetornoFun) ;
    }

   
}

?>