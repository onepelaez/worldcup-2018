<?php

class ConectorMySQLi{
	private $host_bd="";
    private $usuario_bd="";
    private $nom_bd="";
    private $clave_bd="";

	function __construct($host_bd, $nom_bd, $usuario_bd, $clave_bd) {
		$this->host_bd=$host_bd;
        $this->nom_bd=$nom_bd;
        $this->usuario_bd=$usuario_bd;
        $this->clave_bd=$clave_bd;

	}

    public function obtenerConector(){
        $MySQLi =  mysqli_connect($this->host_bd, $this->usuario_bd, $this->clave_bd,$this->nom_bd);
        if (!$MySQLi) {
            die('No pudo conectarse: ' . mysql_error());
        }
        mysqli_set_charset($MySQLi, 'utf8');
        return $MySQLi;
    }

    public function obtenerAssoc($sql){
        $MySQLi=$this->obtenerConector();
        $row=array();
        $rows=array();
        if ($row = $MySQLi->query($sql)) {
            while($fila = mysqli_fetch_array($row, MYSQLI_ASSOC)){
                $rows[]=$fila;
            }        
            $row->free();
            return $rows;
        }
    }

    public function obtenerArray($sql){
        $MySQLi=$this->obtenerConector();
        $row=array();
        if ($row = $MySQLi->query($sql)) {
            while($fila = mysqli_fetch_array($row, MYSQLI_NUM)){
                $rows[]=$fila;
            }        
            $row->free();
            return $rows;
        }
    }

    public function consultar($sql){
        $MySQLi=$this->obtenerConector();
        return $MySQLi->query($sql);
    }

    public function ultimoId(){
        $MySQLi=$this->obtenerConector();
        return $MySQLi->insert_id();
    }
}


?>
