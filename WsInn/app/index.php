<?php
/*
* CREADO 9-12-2017
* Webservice 
* Autor: Andrex Nova
*/

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('log_errors', true);
ini_set('error_log', dirname(__FILE__) . "/error.log");
require_once 'libs/Slim/Slim.php';
require_once(dirname(__FILE__)."/../mods/mod_polla/Pollas.php");
require_once(dirname(__FILE__)."/../mods/mod_usuario/Usuarios.php");

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();


$app->get('/', 
	function () {
		echo "Hola mundo";
	}
);

$app->get('/getJsonHttp', 
	function () {
		$mod_polla= new Pollas();
		$metodoRetorno=$mod_polla->getJsonHttp();
		echo $metodoRetorno;
	}
);

$app->get('/agregarUsuario', 
	function () {

		if(	!isset($_GET['codJugadorPolla'])||
			!isset($_GET['fechapolla'])||
			!isset($_GET['identificacion'])||
			!isset($_GET['nombresJugadorPolla'])||
			!isset($_GET['email'])||
			!isset($_GET['movil']) ){

			echo json_encode(array("status"=>"400",
					"msj"=>"Debe existir GET codJugadorPolla, GET fechapolla , GET identificacion,
					GET nombresJugadorPolla, GET email, GET movil  "));
			die();

		}

		$codJugadorPolla= $_GET['codJugadorPolla'];
		$fechapolla= $_GET['fechapolla'];
		$identificacion= $_GET['identificacion'];
		$nombresJugadorPolla= $_GET['nombresJugadorPolla'];
		$email= $_GET['email'];
		$movil= $_GET['movil'];

					 
		$mod_usuarios= new Usuarios();
		$metodoRetorno=$mod_usuarios->agregarUsuario($codJugadorPolla,$fechapolla,
    					 $identificacion,$nombresJugadorPolla,
    					 $email,$movil);
		echo $metodoRetorno;
	}
);

$app->get('/agregarPollaUsuario', 
	function () {	

	/*
		$_GET['grupos']='  [[1,"Rusia",1,{"groupId":1}],
							[3,"Egipto",2,{"groupId":1}],
							[2,"Arabia Saudita",3,{"groupId":1}],
							[4,"Uruguay",4,{"groupId":1}],
							[5,"Portugal",1,{"groupId":2}],
							[6,"España",2,{"groupId":2}],
							[7,"Marruecos",3,{"groupId":2}],
							[8,"Iran",4,{"groupId":2}],
							[9,"Francia",1,{"groupId":3}],
							[10,"Australia",2,{"groupId":3}],
							[11,"Peru",3,{"groupId":3}],
							[12,"Dinamarca",4,{"groupId":3}],
							[9,"Francia",1,{"groupId":3}],
							[10,"Australia",2,{"groupId":3}],
							[11,"Peru",3,{"groupId":3}],
							[12,"Dinamarca",4,{"groupId":3}],
							[13,"Argentina",1,{"groupId":4}],
							[14,"Islandia",2,{"groupId":4}],
							[16,"Nigeria",3,{"groupId":4}],
							[15,"Croacia",4,{"groupId":4}],
							[17,"Brasil",1,{"groupId":5}],
							[18,"Suiza",2,{"groupId":5}],
							[19,"Costa Rica",3,{"groupId":5}],
							[20,"Serbia",4,{"groupId":5}],
							[21,"Alemania",1,{"groupId":6}],
							[22,"Mexico",2,{"groupId":6}],
							[23,"Suecia",3,{"groupId":6}],
							[24,"Corea del Sur",4,{"groupId":6}],
							[25,"Belgica",1,{"groupId":7}],
							[26,"Panama",2,{"groupId":7}],
							[27,"Tunez",3,{"groupId":7}],
							[28,"Inglaterra",4,{"groupId":7}],
							[29,"Polonia",1,{"groupId":8}],
							[30,"Senegal",2,{"groupId":8}],
							[31,"Colombia",3,{"groupId":8}],
							[32,"Japon",4,{"groupId":8}]]';

 		$_GET['eliminatorias']='  {"teams":[[{"name":"Rusia","flag":""}
										,{"name":"Portugal","flag":""}],[{"name":"Francia","flag":""}
										,{"name":"Croacia","flag":""}],[{"name":"Suiza","flag":""}
										,{"name":"Alemania","flag":""}],[{"name":"Belgica","flag":""}
										,{"name":"Colombia","flag":""}],[{"name":"España","flag":""}
										,{"name":"Uruguay","flag":""}],[{"name":"Argentina","flag":""}
										,{"name":"Australia","flag":""}],[{"name":"Mexico","flag":""}
										,{"name":"Brasil","flag":""}],[{"name":"Polonia","flag":""}
										,{"name":"Inglaterra","flag":""}]],
										"results":[[[[2,3,1,5]
										,[0,4,9,15]
										,[2,6,18,21]
										,[3,0,25,31]
										,[1,0,6,4]
										,[4,5,13,10]
										,[2,1,22,17]
										,[0,2,29,28]]
										,[[0,1,5,15]
										,[3,2,21,25]
										,[1,0,6,10]
										,[7,3,22,28]]
										,[[2,5,15,21]
										,[3,0,6,22]]
										,[[3,1,21,6]
										,[0,3,15,22]
							]]]}';

		$_GET['codJugadorPolla']="XLSMC24Z";
	*/

		if(	!isset($_GET['grupos']) ||
			!isset($_GET['eliminatorias']) ||  
			!isset($_GET['codJugadorPolla']) ){
			echo json_encode(array("status"=>"400",
					"msj"=>"Debe existir GET grupos GET eliminatorias GET codJugadorPolla"));
			die();
		}

		$mod_polla= new Pollas();
		$metodoRetorno=$mod_polla->agregarPollaUsuario($_GET['grupos'],$_GET['eliminatorias'],$_GET['codJugadorPolla']);
		echo $metodoRetorno;
	}
);


$app->get('/verificarUsuario', 
	function () {	

		if(	!isset($_GET['codJugadorPolla']) || !isset($_GET['identificacion']) 
			||  !isset($_GET['email']) ){
			echo json_encode(array("status"=>"400",
					"msj"=>"Debe existir  GET codJugadorPolla GET identificacion GET email"));
			die();
		}


		$mod_usuario= new Usuarios();
		$metodoRetorno=$mod_usuario->verificarUsuario(	$_GET['codJugadorPolla'],
														$_GET['identificacion'],
														$_GET['email']);
		echo $metodoRetorno;
	}
);

$app->get('/puntuacionUsuarios', 
	function () {	

		$mod_usuario= new Usuarios();
		$metodoRetorno=$mod_usuario->puntuacionUsuarios();
		echo $metodoRetorno;
	}
);

$app->get('/actualizarPollas', 
	function () {	

		$mod_pollas= new Pollas();
		$metodoRetorno=$mod_pollas->actualizarPollas();
		echo $metodoRetorno;
	}
);


$app->get('/actualizarPollasJson', 
	function () {	
		$mod_pollas= new Pollas();
		$metodoRetornoJson=$mod_pollas->getJsonHttp();
		$metodoRetorno=$mod_pollas->actualizarPollas();
		echo json_encode(array("GetJson"=>json_decode($metodoRetornoJson),
							   "ActPollas"=>json_decode($metodoRetorno)
							  )
						);
	}
);

$app->run();
