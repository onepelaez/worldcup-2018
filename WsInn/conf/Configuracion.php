<?php
/*
* CREADO 9-12-2017
* Variables de Configuracion del Sistema
* Autor: Andrex Nova
* @class Variables
*/

error_reporting(E_ALL);
ini_set("display_errors", 1);
header('Access-Control-Allow-Origin: *');
set_time_limit(480);
ini_set('memory_limit', '4095M'); // 4 GBs minus 1 MB

	class Variables{
		//Datos basicos de la aplicaci&oacute;n
		public static $NOMBRE_SITIO="Ws Polla Mundialista";
		public static $CHAR_SET="utf-8";
		//Esto se debe generar randomicamente para hacer mas seguro el 
		//sitio y evitar relaciones con el nombre del dominio
		public static $NOMBRE_SESION='ws.InnovationTics';
		//Datos de conexion a la base de datos
		public static $USUARIO_BD="proyectos";
		public static $CLAVE_BD="proyectos";
		public static $HOST_BD="localhost";
		public static $NOMBRE_BD="pollamundialista";
		public static $SERVERLINK="http://innovationtics.xyz/Polla/WsInn/";
	}
?>
